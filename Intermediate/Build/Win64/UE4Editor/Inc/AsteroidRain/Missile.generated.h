// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef ASTEROIDRAIN_Missile_generated_h
#error "Missile.generated.h already included, missing '#pragma once' in Missile.h"
#endif
#define ASTEROIDRAIN_Missile_generated_h

#define AsteroidRain_Source_AsteroidRain_Missile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define AsteroidRain_Source_AsteroidRain_Missile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define AsteroidRain_Source_AsteroidRain_Missile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMissile(); \
	friend struct Z_Construct_UClass_AMissile_Statics; \
public: \
	DECLARE_CLASS(AMissile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AsteroidRain"), NO_API) \
	DECLARE_SERIALIZER(AMissile)


#define AsteroidRain_Source_AsteroidRain_Missile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMissile(); \
	friend struct Z_Construct_UClass_AMissile_Statics; \
public: \
	DECLARE_CLASS(AMissile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AsteroidRain"), NO_API) \
	DECLARE_SERIALIZER(AMissile)


#define AsteroidRain_Source_AsteroidRain_Missile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMissile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMissile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMissile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMissile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMissile(AMissile&&); \
	NO_API AMissile(const AMissile&); \
public:


#define AsteroidRain_Source_AsteroidRain_Missile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMissile(AMissile&&); \
	NO_API AMissile(const AMissile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMissile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMissile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMissile)


#define AsteroidRain_Source_AsteroidRain_Missile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(AMissile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__Mesh() { return STRUCT_OFFSET(AMissile, Mesh); } \
	FORCEINLINE static uint32 __PPO__Movement() { return STRUCT_OFFSET(AMissile, Movement); }


#define AsteroidRain_Source_AsteroidRain_Missile_h_9_PROLOG
#define AsteroidRain_Source_AsteroidRain_Missile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AsteroidRain_Source_AsteroidRain_Missile_h_12_PRIVATE_PROPERTY_OFFSET \
	AsteroidRain_Source_AsteroidRain_Missile_h_12_RPC_WRAPPERS \
	AsteroidRain_Source_AsteroidRain_Missile_h_12_INCLASS \
	AsteroidRain_Source_AsteroidRain_Missile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AsteroidRain_Source_AsteroidRain_Missile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AsteroidRain_Source_AsteroidRain_Missile_h_12_PRIVATE_PROPERTY_OFFSET \
	AsteroidRain_Source_AsteroidRain_Missile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	AsteroidRain_Source_AsteroidRain_Missile_h_12_INCLASS_NO_PURE_DECLS \
	AsteroidRain_Source_AsteroidRain_Missile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AsteroidRain_Source_AsteroidRain_Missile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
