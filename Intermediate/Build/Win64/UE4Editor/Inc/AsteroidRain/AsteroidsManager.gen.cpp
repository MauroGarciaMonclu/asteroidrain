// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AsteroidRain/AsteroidsManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAsteroidsManager() {}
// Cross Module References
	ASTEROIDRAIN_API UClass* Z_Construct_UClass_AAsteroidsManager_NoRegister();
	ASTEROIDRAIN_API UClass* Z_Construct_UClass_AAsteroidsManager();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_AsteroidRain();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
// End Cross Module References
	void AAsteroidsManager::StaticRegisterNativesAAsteroidsManager()
	{
	}
	UClass* Z_Construct_UClass_AAsteroidsManager_NoRegister()
	{
		return AAsteroidsManager::StaticClass();
	}
	struct Z_Construct_UClass_AAsteroidsManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AsteroidsPerSecond_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AsteroidsPerSecond;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Y_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Y;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_X_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_X;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpawnLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RootLocation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAsteroidsManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_AsteroidRain,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAsteroidsManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AsteroidsManager.h" },
		{ "ModuleRelativePath", "AsteroidsManager.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_AsteroidsPerSecond_MetaData[] = {
		{ "Category", "AsteroidsManager" },
		{ "ModuleRelativePath", "AsteroidsManager.h" },
		{ "ToolTip", "Frecuency with asteroid spawn per second" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_AsteroidsPerSecond = { UE4CodeGen_Private::EPropertyClass::Float, "AsteroidsPerSecond", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000801, 1, nullptr, STRUCT_OFFSET(AAsteroidsManager, AsteroidsPerSecond), METADATA_PARAMS(Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_AsteroidsPerSecond_MetaData, ARRAY_COUNT(Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_AsteroidsPerSecond_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_Y_MetaData[] = {
		{ "Category", "AsteroidsManager" },
		{ "ModuleRelativePath", "AsteroidsManager.h" },
		{ "ToolTip", "Size of component Y to spawn asteroids" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_Y = { UE4CodeGen_Private::EPropertyClass::Float, "Y", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000801, 1, nullptr, STRUCT_OFFSET(AAsteroidsManager, Y), METADATA_PARAMS(Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_Y_MetaData, ARRAY_COUNT(Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_Y_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_X_MetaData[] = {
		{ "Category", "AsteroidsManager" },
		{ "ModuleRelativePath", "AsteroidsManager.h" },
		{ "ToolTip", "Size of component X to spawn asteroids" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_X = { UE4CodeGen_Private::EPropertyClass::Float, "X", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000801, 1, nullptr, STRUCT_OFFSET(AAsteroidsManager, X), METADATA_PARAMS(Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_X_MetaData, ARRAY_COUNT(Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_X_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_SpawnLocation_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Spawn" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "AsteroidsManager.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_SpawnLocation = { UE4CodeGen_Private::EPropertyClass::Object, "SpawnLocation", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(AAsteroidsManager, SpawnLocation), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_SpawnLocation_MetaData, ARRAY_COUNT(Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_SpawnLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_RootLocation_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Root" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "AsteroidsManager.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_RootLocation = { UE4CodeGen_Private::EPropertyClass::Object, "RootLocation", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(AAsteroidsManager, RootLocation), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_RootLocation_MetaData, ARRAY_COUNT(Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_RootLocation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AAsteroidsManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_AsteroidsPerSecond,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_Y,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_X,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_SpawnLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAsteroidsManager_Statics::NewProp_RootLocation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAsteroidsManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAsteroidsManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AAsteroidsManager_Statics::ClassParams = {
		&AAsteroidsManager::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		Z_Construct_UClass_AAsteroidsManager_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_AAsteroidsManager_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AAsteroidsManager_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AAsteroidsManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAsteroidsManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AAsteroidsManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAsteroidsManager, 1751798871);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAsteroidsManager(Z_Construct_UClass_AAsteroidsManager, &AAsteroidsManager::StaticClass, TEXT("/Script/AsteroidRain"), TEXT("AAsteroidsManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAsteroidsManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
