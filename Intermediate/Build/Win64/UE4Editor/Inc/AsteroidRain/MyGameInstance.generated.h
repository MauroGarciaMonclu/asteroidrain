// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASTEROIDRAIN_MyGameInstance_generated_h
#error "MyGameInstance.generated.h already included, missing '#pragma once' in MyGameInstance.h"
#endif
#define ASTEROIDRAIN_MyGameInstance_generated_h

#define AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetScore) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Score); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetScore(Z_Param_Score); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetScore) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetScore(); \
		P_NATIVE_END; \
	}


#define AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetScore) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Score); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetScore(Z_Param_Score); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetScore) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetScore(); \
		P_NATIVE_END; \
	}


#define AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyGameInstance(); \
	friend struct Z_Construct_UClass_UMyGameInstance_Statics; \
public: \
	DECLARE_CLASS(UMyGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/AsteroidRain"), NO_API) \
	DECLARE_SERIALIZER(UMyGameInstance)


#define AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUMyGameInstance(); \
	friend struct Z_Construct_UClass_UMyGameInstance_Statics; \
public: \
	DECLARE_CLASS(UMyGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/AsteroidRain"), NO_API) \
	DECLARE_SERIALIZER(UMyGameInstance)


#define AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyGameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyGameInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyGameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyGameInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyGameInstance(UMyGameInstance&&); \
	NO_API UMyGameInstance(const UMyGameInstance&); \
public:


#define AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyGameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyGameInstance(UMyGameInstance&&); \
	NO_API UMyGameInstance(const UMyGameInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyGameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyGameInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyGameInstance)


#define AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_PRIVATE_PROPERTY_OFFSET
#define AsteroidRain_Source_AsteroidRain_MyGameInstance_h_9_PROLOG
#define AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_PRIVATE_PROPERTY_OFFSET \
	AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_RPC_WRAPPERS \
	AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_INCLASS \
	AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_PRIVATE_PROPERTY_OFFSET \
	AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_INCLASS_NO_PURE_DECLS \
	AsteroidRain_Source_AsteroidRain_MyGameInstance_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AsteroidRain_Source_AsteroidRain_MyGameInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
