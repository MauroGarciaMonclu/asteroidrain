// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASTEROIDRAIN_AsteroidRainGameModeBase_generated_h
#error "AsteroidRainGameModeBase.generated.h already included, missing '#pragma once' in AsteroidRainGameModeBase.h"
#endif
#define ASTEROIDRAIN_AsteroidRainGameModeBase_generated_h

#define AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_RPC_WRAPPERS
#define AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAsteroidRainGameModeBase(); \
	friend struct Z_Construct_UClass_AAsteroidRainGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAsteroidRainGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/AsteroidRain"), NO_API) \
	DECLARE_SERIALIZER(AAsteroidRainGameModeBase)


#define AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAsteroidRainGameModeBase(); \
	friend struct Z_Construct_UClass_AAsteroidRainGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAsteroidRainGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/AsteroidRain"), NO_API) \
	DECLARE_SERIALIZER(AAsteroidRainGameModeBase)


#define AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAsteroidRainGameModeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAsteroidRainGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAsteroidRainGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAsteroidRainGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAsteroidRainGameModeBase(AAsteroidRainGameModeBase&&); \
	NO_API AAsteroidRainGameModeBase(const AAsteroidRainGameModeBase&); \
public:


#define AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAsteroidRainGameModeBase(AAsteroidRainGameModeBase&&); \
	NO_API AAsteroidRainGameModeBase(const AAsteroidRainGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAsteroidRainGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAsteroidRainGameModeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAsteroidRainGameModeBase)


#define AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_PRIVATE_PROPERTY_OFFSET
#define AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_9_PROLOG
#define AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_PRIVATE_PROPERTY_OFFSET \
	AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_RPC_WRAPPERS \
	AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_INCLASS \
	AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_PRIVATE_PROPERTY_OFFSET \
	AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_INCLASS_NO_PURE_DECLS \
	AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AsteroidRain_Source_AsteroidRain_AsteroidRainGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
