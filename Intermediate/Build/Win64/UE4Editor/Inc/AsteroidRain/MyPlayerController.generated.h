// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASTEROIDRAIN_MyPlayerController_generated_h
#error "MyPlayerController.generated.h already included, missing '#pragma once' in MyPlayerController.h"
#endif
#define ASTEROIDRAIN_MyPlayerController_generated_h

#define AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetScore) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetScore(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLife) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetLife(); \
		P_NATIVE_END; \
	}


#define AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetScore) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetScore(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetLife) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetLife(); \
		P_NATIVE_END; \
	}


#define AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyPlayerController(); \
	friend struct Z_Construct_UClass_AMyPlayerController_Statics; \
public: \
	DECLARE_CLASS(AMyPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AsteroidRain"), NO_API) \
	DECLARE_SERIALIZER(AMyPlayerController)


#define AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMyPlayerController(); \
	friend struct Z_Construct_UClass_AMyPlayerController_Statics; \
public: \
	DECLARE_CLASS(AMyPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AsteroidRain"), NO_API) \
	DECLARE_SERIALIZER(AMyPlayerController)


#define AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyPlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyPlayerController(AMyPlayerController&&); \
	NO_API AMyPlayerController(const AMyPlayerController&); \
public:


#define AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyPlayerController(AMyPlayerController&&); \
	NO_API AMyPlayerController(const AMyPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyPlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyPlayerController)


#define AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define AsteroidRain_Source_AsteroidRain_MyPlayerController_h_9_PROLOG
#define AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_RPC_WRAPPERS \
	AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_INCLASS \
	AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_INCLASS_NO_PURE_DECLS \
	AsteroidRain_Source_AsteroidRain_MyPlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AsteroidRain_Source_AsteroidRain_MyPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
